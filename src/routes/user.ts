import { Router } from "express";
import passport from "passport";

import Role from "../enums/Role";
import userController from "../controllers/UserController";
import { hasRole, response } from "../middlewares";

const router = Router();

router.get(
  "/",
  passport.authenticate("jwt", { session: false }),
  hasRole(Role.ADMIN),
  userController.getAll,
  response
);

router.post(
  "/",
  passport.authenticate("jwt", { session: false }),
  hasRole(Role.ADMIN),
  userController.create,
  response
);

router.get(
  "/:id",
  passport.authenticate("jwt", { session: false }),
  hasRole(Role.ADMIN),
  userController.getOne,
  response
);

router.put(
  "/:id",
  passport.authenticate("jwt", { session: false }),
  hasRole(Role.ADMIN),
  userController.update,
  response
);

router.delete(
  "/:id",
  passport.authenticate("jwt", { session: false }),
  hasRole(Role.ADMIN),
  userController.delete,
  response
);

export default router;
