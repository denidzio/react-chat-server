import { Express } from "express-serve-static-core";
import authRoutes from "./auth";
import userRoutes from "./user";
import messageRoutes from "./message";
import userMessage from "./userMessage";

const router = (app: Express) => {
  app.use("/api/auth", authRoutes);
  app.use("/api/user", userRoutes, userMessage);
  app.use("/api/message", messageRoutes);
};

export default router;
