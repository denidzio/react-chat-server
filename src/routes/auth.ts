import { Router } from "express";

import authController from "../controllers/AuthController";
import { response } from "../middlewares";

const router = Router();

router.post("/register", authController.register, response);
router.post("/login", authController.login, response);

export default router;
