import { Router } from "express";
import passport from "passport";

import Role from "../enums/Role";
import messageController from "../controllers/MessageController";
import { hasRole, response } from "../middlewares";

const router = Router();

router.get(
  "/",
  passport.authenticate("jwt", { session: false }),
  messageController.getAll,
  response
);

router.post(
  "/",
  passport.authenticate("jwt", { session: false }),
  hasRole(Role.ADMIN),
  messageController.create,
  response
);

router.get(
  "/:id",
  passport.authenticate("jwt", { session: false }),
  messageController.getOne,
  response
);

router.put(
  "/:id",
  passport.authenticate("jwt", { session: false }),
  hasRole(Role.ADMIN),
  messageController.update,
  response
);

router.delete(
  "/:id",
  passport.authenticate("jwt", { session: false }),
  hasRole(Role.ADMIN),
  messageController.delete,
  response
);

export default router;
