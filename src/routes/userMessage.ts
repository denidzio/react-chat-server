import { Router } from "express";
import passport from "passport";

import Role from "../enums/Role";
import userMessageController from "../controllers/UserMessageController";
import { hasRole, response } from "../middlewares";

const router = Router();

router.post(
  "/message",
  passport.authenticate("jwt", { session: false }),
  hasRole(Role.USER),
  userMessageController.sendMessage,
  response
);

router.patch(
  "/message/:id",
  passport.authenticate("jwt", { session: false }),
  hasRole(Role.USER),
  userMessageController.likeMessage,
  response
);

router.put(
  "/message/:id",
  passport.authenticate("jwt", { session: false }),
  hasRole(Role.USER),
  userMessageController.editMessage,
  response
);

router.delete(
  "/message/:id",
  passport.authenticate("jwt", { session: false }),
  hasRole(Role.USER),
  userMessageController.deleteMessage,
  response
);

export default router;
