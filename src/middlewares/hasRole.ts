import { NextFunction, Request, Response } from "express";

import { IUser } from "../interfaces";
import Role from "../enums/Role";

export default function hasRole(role: Role) {
  return (req: Request, res: Response, next: NextFunction) => {
    const user = req.user as IUser;

    if (!user || !user.roles.includes(role)) {
      res.locals.error = { code: 403, message: "Forbidden." };
    }

    next();
  };
}
