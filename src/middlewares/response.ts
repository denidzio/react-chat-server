import { NextFunction, Request, Response } from "express";

export default function response(
  req: Request,
  res: Response,
  next: NextFunction
) {
  if (res.locals.error) {
    const { code, message } = res.locals.error;
    return res.status(code).json({ error: true, message });
  }

  const { code, data } = res.locals.success;
  return res.status(code || 200).json(data);
}
