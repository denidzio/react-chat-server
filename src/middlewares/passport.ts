import { PassportStatic } from "passport";
import { Strategy, ExtractJwt } from "passport-jwt";
import { JWT } from "../config";
import User from "../models/User";

const options = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
  secretOrKey: JWT,
};

export default function (passport: PassportStatic) {
  passport.use(
    new Strategy(options, async (payload, done) => {
      try {
        const user = await User.findById(payload.id);

        if (user) {
          done(null, user);
        } else {
          done(null, false);
        }
      } catch (e) {
        console.log(e);
      }
    })
  );
}
