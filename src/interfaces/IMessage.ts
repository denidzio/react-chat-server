import IUser from "./IUser";

interface IMessage {
  _id: string;
  user: { _id: string; name: string; avatar: SVGStringList };
  text: string;
  liked: boolean;
  createdAt: string;
  modifiedAt: string;
}

export default IMessage;
