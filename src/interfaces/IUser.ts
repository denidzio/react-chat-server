interface IUser {
  _id: string;
  name: string;
  password: string;
  avatar: string;
  roles: string[];
}

export default IUser;
