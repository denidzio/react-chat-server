import path from "path";
import { createServer } from "http";
import express from "express";
import cors from "cors";
import mongoose from "mongoose";
import passport from "passport";
import { Server } from "socket.io";

import { PORT, STATIC_PATH } from "./config";
import { URI } from "./db/config";
import routes from "./routes";
import passportJwt from "./middlewares/passport";

const app = express();
const httpServer = createServer(app);
const io = new Server(httpServer, {
  cors: {
    origin: "http://localhost:3000",
    methods: ["GET", "POST"],
  },
});

mongoose
  .connect(URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
  })
  .then((_) => console.log("MongoDB connected."))
  .catch(console.log);

app.use(express.static(STATIC_PATH));
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(passport.initialize());

passportJwt(passport);
routes(app);

app.get("*", (req, res) => {
  res.sendFile(path.join(STATIC_PATH, "index.html"));
});

httpServer.listen(process.env.PORT || PORT, () => {
  console.log(`Server has been launched on port ${PORT}`);
});

export { io };
