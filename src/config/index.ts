import path from "path";

export const PORT = 3002;
export const STATIC_PATH = path.join(__dirname, "..", "public");
export const JWT = "chat";
