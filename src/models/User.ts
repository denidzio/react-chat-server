import { Schema, model } from "mongoose";
import { IUser } from "../interfaces";

export const User = new Schema<IUser>({
  name: { type: String, required: true },
  password: { type: String, required: true },
  roles: { type: Array, required: true },
  avatar: { type: String },
  createdAt: { type: String, default: "" },
  modifiedAt: { type: String, default: "" },
});

export default model<IUser>("User", User);
