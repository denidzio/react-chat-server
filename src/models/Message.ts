import { Schema, model } from "mongoose";

import IMessage from "../interfaces/IMessage";

export const schema = new Schema<IMessage>({
  user: { _id: String, name: String, avatar: String },
  text: { type: String, required: true },
  liked: { type: Boolean, required: true },
  createdAt: { type: String, default: "" },
  modifiedAt: { type: String, default: "" },
});

export default model<IMessage>("Message", schema);
