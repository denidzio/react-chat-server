import { NextFunction, Request, Response } from "express";
import Message from "../models/Message";

class MessageController {
  async getAll(req: Request, res: Response, next: NextFunction) {
    try {
      if (res.locals.error) {
        return next();
      }

      res.locals.success = { code: 200, data: await Message.find({}) };
      next();
    } catch (e) {
      throw new Error(e);
    }
  }

  async create(req: Request, res: Response, next: NextFunction) {
    try {
      if (res.locals.error) {
        return next();
      }

      const message = new Message({
        ...req.body,
        createdAt: new Date().toISOString(),
      });

      res.locals.success = { code: 200, data: await message.save() };
      next();
    } catch (e) {
      throw new Error(e);
    }
  }

  async getOne(req: Request, res: Response, next: NextFunction) {
    try {
      if (res.locals.error) {
        return next();
      }

      const message = await Message.findById(req.params.id);

      if (message === null) {
        res.locals.error = { code: 404, message: "Message is not found." };
        return next();
      }

      res.locals.success = {
        code: 200,
        data: message,
      };

      next();
    } catch (e) {
      throw new Error(e);
    }
  }

  async update(req: Request, res: Response, next: NextFunction) {
    try {
      if (res.locals.error) {
        return next();
      }

      const message = await Message.findById(req.params.id);

      if (message === null) {
        res.locals.error = { code: 404, message: "Message is not found." };
        return next();
      }

      req.body.modifiedAt = new Date().toISOString();

      const updatedMessage = await Message.findByIdAndUpdate(
        req.params.id,
        req.body
      );

      res.locals.success = { code: 200, data: updatedMessage };

      next();
    } catch (e) {
      throw new Error(e);
    }
  }

  async delete(req: Request, res: Response, next: NextFunction) {
    try {
      if (res.locals.error) {
        return next();
      }

      const message = await Message.findById(req.params.id);

      if (message === null) {
        res.locals.error = { code: 404, message: "Message is not found." };
        return next();
      }

      res.locals.success = {
        code: 200,
        data: await Message.findByIdAndDelete(req.params.id),
      };

      next();
    } catch (e) {
      throw new Error(e);
    }
  }
}

export default new MessageController();
