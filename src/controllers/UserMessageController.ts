import { NextFunction, Request, Response } from "express";
import { IUser } from "../interfaces";
import Message from "../models/Message";
import { io } from "../server";

class UserMessageController {
  async sendMessage(req: Request, res: Response, next: NextFunction) {
    try {
      if (res.locals.error) {
        return next();
      }

      const message = new Message({
        text: req.body.text,
        user: req.user,
        liked: false,
        createdAt: new Date().toISOString(),
      });

      res.locals.success = { code: 200, data: await message.save() };
      io.emit("UPDATE_MESSAGES");
      next();
    } catch (e) {
      throw new Error(e);
    }
  }

  async editMessage(req: Request, res: Response, next: NextFunction) {
    try {
      if (res.locals.error) {
        return next();
      }

      const user = req.user as IUser;
      const message = await Message.findById(req.params.id);

      if (message === null) {
        res.locals.error = { code: 404, message: "Message is not found." };
        return next();
      }

      if (message.user._id.toString() !== user._id.toString()) {
        res.locals = { code: 403, message: "Forbidden." };
        return next();
      }

      message.text = req.body.text;
      message.modifiedAt = new Date().toISOString();

      res.locals.success = { code: 200, data: await message.save() };
      io.emit("UPDATE_MESSAGES");
      next();
    } catch (e) {
      throw new Error(e);
    }
  }

  async likeMessage(req: Request, res: Response, next: NextFunction) {
    try {
      if (res.locals.error) {
        return next();
      }

      const user = req.user as IUser;
      const message = await Message.findById(req.params.id);

      if (message === null) {
        res.locals.error = { code: 404, message: "Message is not found." };
        return next();
      }

      if (message.user._id.toString() === user._id.toString()) {
        res.locals = { code: 403, message: "Forbidden." };
        return next();
      }

      message.liked = !message.liked;
      message.modifiedAt = new Date().toISOString();

      res.locals.success = { code: 200, data: await message.save() };
      io.emit("UPDATE_MESSAGES");
      next();
    } catch (e) {
      throw new Error(e);
    }
  }

  async deleteMessage(req: Request, res: Response, next: NextFunction) {
    try {
      if (res.locals.error) {
        return next();
      }

      const user = req.user as IUser;
      const message = await Message.findById(req.params.id);

      if (message === null) {
        res.locals.error = { code: 404, message: "Message is not found." };
        return next();
      }

      if (message.user._id.toString() !== user._id.toString()) {
        res.locals.error = { code: 403, message: "Forbidden." };
        return next();
      }

      res.locals.success = {
        code: 200,
        data: await Message.findByIdAndDelete(req.params.id),
      };

      io.emit("UPDATE_MESSAGES");
      next();
    } catch (e) {
      throw new Error(e);
    }
  }
}

export default new UserMessageController();
