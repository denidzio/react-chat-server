import { NextFunction, Request, Response } from "express";
import bcrypt from "bcryptjs";
import jwt from "jsonwebtoken";

import { JWT } from "../config";
import User from "../models/User";
import Role from "../enums/Role";

class AuthController {
  async register(req: Request, res: Response, next: NextFunction) {
    try {
      if (res.locals.error) {
        return next();
      }

      const candidate = await User.findOne({ name: req.body.name });

      if (candidate) {
        res.locals.error = { code: 409, message: "User name is already busy." };
        return next();
      }

      const hash = bcrypt.genSaltSync(10);

      const user = new User({
        name: req.body.name,
        password: bcrypt.hashSync(req.body.password, hash),
        avatar: req.body.avatar,
        roles: [Role.USER],
        createdAt: new Date().toISOString(),
      });

      const createdUser = await user.save();

      res.locals.success = { code: 201, data: createdUser };
      next();
    } catch (e) {
      throw new Error(e);
    }
  }

  async login(req: Request, res: Response, next: NextFunction) {
    try {
      if (res.locals.error) {
        return next();
      }

      const candidate = await User.findOne({ name: req.body.name });

      if (!candidate) {
        res.locals.error = { code: 404, message: "User is not found." };
        return next();
      }

      const passwordResult = bcrypt.compareSync(
        req.body.password,
        candidate.password
      );

      if (!passwordResult) {
        res.locals.error = { code: 401, message: "Password is incorrect." };
        return next();
      }

      const token = jwt.sign({ id: candidate._id }, JWT, {
        expiresIn: "24h",
      });

      res.locals.success = {
        code: 200,
        data: { token, name: candidate.name, roles: candidate.roles },
      };

      next();
    } catch (e) {
      throw new Error(e);
    }
  }
}

export default new AuthController();
