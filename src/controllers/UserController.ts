import { NextFunction, Request, Response } from "express";
import bcrypt from "bcryptjs";
import User from "../models/User";

class UserController {
  async getAll(req: Request, res: Response, next: NextFunction) {
    try {
      if (res.locals.error) {
        return next();
      }

      res.locals.success = { code: 200, data: await User.find({}) };
      next();
    } catch (e) {
      throw new Error(e);
    }
  }

  async create(req: Request, res: Response, next: NextFunction) {
    try {
      if (res.locals.error) {
        return next();
      }

      const user = new User({
        ...req.body,
        createdAt: new Date().toISOString(),
      });

      res.locals.success = { code: 200, data: await user.save() };
      next();
    } catch (e) {
      throw new Error(e);
    }
  }

  async getOne(req: Request, res: Response, next: NextFunction) {
    try {
      if (res.locals.error) {
        return next();
      }

      const user = await User.findById(req.params.id);

      if (user === null) {
        res.locals.error = { code: 404, message: "User is not found." };
        return next();
      }

      res.locals.success = {
        code: 200,
        data: user,
      };

      next();
    } catch (e) {
      throw new Error(e);
    }
  }

  async update(req: Request, res: Response, next: NextFunction) {
    try {
      if (res.locals.error) {
        return next();
      }

      const user = await User.findById(req.params.id);

      if (user === null) {
        res.locals.error = { code: 404, message: "User is not found" };
        return next();
      }

      if (req.body.password) {
        const hash = bcrypt.genSaltSync(10);
        req.body.password = bcrypt.hashSync(req.body.password, hash);
      }

      req.body.modifiedAt = new Date().toISOString();
      const updatedUser = await User.findByIdAndUpdate(req.params.id, req.body);

      res.locals.success = { code: 200, data: updatedUser };
      next();
    } catch (e) {
      throw new Error(e);
    }
  }

  async delete(req: Request, res: Response, next: NextFunction) {
    try {
      if (res.locals.error) {
        return next();
      }

      const userWillDeleted = await User.findById(req.params.id);

      if (userWillDeleted === null) {
        res.locals.error = { code: 404, message: "User is not found." };
        return next();
      }

      res.locals.success = {
        code: 200,
        data: await User.findByIdAndDelete(req.params.id),
      };

      next();
    } catch (e) {
      throw new Error(e);
    }
  }
}

export default new UserController();
